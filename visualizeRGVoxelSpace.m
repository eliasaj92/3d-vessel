function visualizeRGVoxelSpace(X,Y,Z,R,RedLevel)

%quick script to visualize the red channel

hf=figure;

% Red
p1 = patch(isosurface(X, Y, Z, R, RedLevel));
isonormals(X,Y,Z,R, p1);
set(p1,'FaceColor','red');
set(p1,'EdgeColor','none');


% Viz params
daspect([100 100 100])
view(3)
camlight; lighting phong;
set(hf,'Color','w');
set(gca,'FontSize',16);
xlabel('x (\mu m)','FontSize',16);
ylabel('y (\mu m)','FontSize',16);
zlabel('z (\mu m)','FontSize',16);

end

