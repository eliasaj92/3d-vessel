function [VesselSkeleton, VesselDir, polyform] = create_branch_segment3D...
    (Vessel, NBigSteps, npoints, segment_size, u1, Points, conformity);

extra = 0.0;

step = segment_size/(2*NBigSteps);

xp = Points(1,1);
yp = Points(2,1);
zp = Points(3,1);
m = size(Vessel,2);

xRange = sqrt(segment_size*segment_size*(0.3+0.1*rand(1,1)));
yRange = sqrt(segment_size*segment_size*(0.3+0.1*rand(1,1)));
zRange = sqrt(segment_size*segment_size - yRange*yRange - xRange*xRange);

n = 1;

while (n <=NBigSteps)
    
    
    new_xp = step*u1(1,1) + xp + extra*randn*xRange; 
    new_yp = step*u1(1,2) + yp + extra*randn*yRange;
    new_zp = step*u1(1,3) + zp + extra*randn*zRange;
    
    xp = new_xp;
    yp = new_yp;
    zp = new_zp;
    u1 = getValidDirection3D(u1, conformity);
    Points=[Points,[xp;yp; zp]];
    
    n = n+1;
    
end


ratio = n/NBigSteps;%/(2^step_reduction);
npoints = ratio*npoints;

t = linspace(0,1,size(Points,2));
tt = linspace(0,1,npoints);


polyform = csapi(t, Points);
polyform.addToArray = 1;
VesselSkeleton = fnval(polyform,tt);

p1 = fnder(polyform,1);
VesselDir = fnval(p1,tt);



end