function [d_ratio, loc_dr] = distanceFromVessel3DMesh(Vessel,...
    x, y,z, Radius_Array)

%This code calculates the distance of each pixel from the vessel
%This uses mesh grids for calculations of the minimum

[X,Y,Z] = meshgrid(x,y,z);

c = size(Vessel,2);


d_ratio = inf*ones(size(X)); %supposed ratios to vessels
d2vessel = inf*ones(size(X)); %distance to vessel

loc_dv = ones(size(X)); %stores index of point on vessel
loc_dr = ones(size(X));


%The following is a fix to account for when the radius is too small
%compared to the distance between two spline points
%This arising problem makes it that there are small blobs of intensity
%across the vessel as the radius becomes too small

temp = [0;0;0]; %can't decide what value to assign to the first variable
temp = [temp, Vessel(1:3,1:end-1)];

diff = Vessel(1:3,:) - temp; %this is supposed to be the vectors 
%between consectuive points 
%note that an error occurs when we finish off a "main" vessel 

%the following lines are a fix for this

for i = 2:size(Vessel,2)
    if(Vessel(4,i-1)~= Vessel(4,i))
        if (Vessel(4,i-1) ~= (Vessel(6,i)) )
        
            ind2 = Vessel(6,i) == Vessel(4,:);
            temp_array = Vessel(:,ind2);
            temp_point = temp_array(:,end);
            diff(1:3,i) = Vessel(1:3,i) - temp_point(1:3,1);
        
        end
    end
end

point_dist = zeros(1,size(diff,2)); %supposed to be the distance between 
%two points in the spline
%in this case pointdist is between point i and i-1


for n = 1:size(diff,2)

    point_dist(1,n) = norm(diff(:,n));
end


%the following is the intesity calculations employing the fix

for i = 2:c %exclude first point
    Rad = assignRadiusToVoxel(i, Radius_Array);
    mx = Vessel(1,i);
    my = Vessel(2,i);
    mz = Vessel(3,i);
    diff_X = X-mx;
    diff_Y = Y-my;
    diff_Z = Z-mz;
    tdist = sqrt((diff_X).*(diff_X) + (diff_Y).*(diff_Y) + (diff_Z).*(diff_Z) );
    
    proj = (diff_X*diff(1,i)+diff_Y*diff(2,i)+diff_Z*diff(3,i)); %projecting onto 
    %the straightline to get the distance from the line is the main fix
    t_cos = (proj)./tdist/point_dist(1,i); %this value is NaN at branching points branching points duplicate for each segment
    t_sin = sqrt(1 - t_cos.*t_cos); 
    tdist_fixed = tdist.*t_sin; %fix the distance

    ind = proj<point_dist(1,i) & proj>0;% if projection is
    %bigger than the distance between the two points or negative 
    %then don't take into account the fix
    tdist(ind) = tdist_fixed(ind); %fix the distance where applicable
    
    tdist_r = tdist/Rad; %take the distance to radius ratio
    ind_r = tdist_r < d_ratio; %find the minimum of these ratios
    d_ratio(ind_r) = tdist_r(ind_r); 
    loc_dr(ind_r) = i;
end


end