function [u]  = getValidDirection3D(v,conformity)

%Gets a direction that more or less moves forward from the previous
%direction. Some deviation allowed as a straight tube vessel isn't
%interesting

 %increase if you want a straighter vessel. 
%Decerease to make it curve more
v = v/norm(v); %normalize v
NoValidDirection = 1;
while NoValidDirection 
    u = makerandunitdirvec(1);
    dp = v*u';
    if dp>conformity
        NoValidDirection = 0;
    end
end
end

function u1=makerandunitdirvec(N)
v = randn(N,3);
u1 = bsxfun(@rdivide,v,sqrt(sum(v.^2,2)));
end