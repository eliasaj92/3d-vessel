function [VesselSkeleton, Radius_Array, polyform] = create_3D_vessel...
    (npoints, NBigSteps,...
    butterOrder, segment_size, Radius, BigAng, SmallAng, conformity, ...
    dx , dy ,dz,Noise )


%Code to generate 3D vessels

xlocs = dx*(-npoints/2:npoints/2);
ylocs = dy*(-npoints/2:npoints/2);
zlocs = dz*(-npoints/2:npoints/2);


xRange = xlocs(end)-xlocs(1);
yRange = ylocs(end)-ylocs(1);
zRange = zlocs(end)-zlocs(1);


[xp,yp, zp, v] =  setStartSideAndDir3D(xlocs,ylocs,zlocs);
Points = [xp;yp;zp];


u1  = getValidDirection3D(v,conformity );
VesselDir(1,:) = u1';

[VesselSkeleton, VesselDir, polyform] = create_branch_segment3D...
    ([], NBigSteps, npoints, segment_size, u1, Points, conformity);

polyform.id = 1; %this is the ID of the vessel segment, to be used to 
%to create structure

v_length = size(VesselSkeleton, 2); %main branch length

Radius_Rank = ones(1, v_length); % radius rank is used to determine 
%radius to be used at point
Branch_rank = ones(1,v_length); %branch length is used to determine 
%length of branch at point

Parent = zeros(1,size(VesselSkeleton,2)); 

VesselSkeleton = [VesselSkeleton; Radius_Rank; Branch_rank];

Radius_Array = [Radius; 1]; % array of the id of branch with its
%corresponding radius value
temp_Radius = Radius;%Radius to use for calcuations of daughter vessel radii

temp_br = 1; %we use this value to determine what level of branching
%the algorithm is currently at. A value of 1 indicates the "main" vessel

theta1 = SmallAng*pi/180; %convert to radian
theta2 = BigAng*pi/180; %convert to radians


u2 = [];

i = 1;

while ((i<v_length+1) & (i <500))

    %temp variables to generate second branch of bifurcation 
     branch1 = []; %array for our first new branch points
     branch_dir1 = []; %this will be needed to get later branches
     rad_r1 = [];
     branch_r1 = [];
     
     %temp variables to generate second branch of bifurcation
     branch2 = []; %array for our second new  branch points
     branch_dir2 = [];
     rad_r2 = [];
     branch_r2 = [];
     
     
     [temp_br, t_npoints, t_size, NBigSteps] = InitializeTemps3D(...
         VesselSkeleton, i, npoints, segment_size, NBigSteps);
     
     if (i == v_length) | (VesselSkeleton(4,i) ~= VesselSkeleton(4,i+1))
         
         temp_Radius = assignRadiusToVoxel(i,Radius_Array);
         [R1, R2] = assignRadius(temp_Radius);
        
        [u1, u2] = getBifurcationDirections3D(VesselDir(:,i)',theta1,...
              theta2); %want our new branch to be in another 
         %direction than where we are branching off
          
         
         Points = [VesselSkeleton(1,i);VesselSkeleton(2,i);VesselSkeleton(3,i)];
        
         [branch1, branch_dir1, temp_poly1] =  create_branch_segment3D...
            (VesselSkeleton,NBigSteps, npoints, segment_size, u1,...
            Points, conformity);
        if (size(branch1) ~= size([]))
             
             [VesselSkeleton, VesselDir,Parent, polyform] = AddBranch3D(VesselSkeleton,...
                VesselDir,polyform, Parent, temp_br,branch1, branch_dir1, temp_poly1,...
                i);
             R1 = [R1; v_length+1]; %takes index of branching point
             Radius_Array = [Radius_Array, R1];
             v_length = size(VesselSkeleton,2);
             
             
         end
         
        %first branch created now onto the second     
        [branch2, branch_dir2, temp_poly2] =  create_branch_segment3D...
        (VesselSkeleton,NBigSteps, npoints, segment_size/1.3, u2, Points, conformity) ;
    
        if (size(branch2) ~= size([]))
            [VesselSkeleton, VesselDir,Parent, polyform] = AddBranch3D(VesselSkeleton,...
            VesselDir,polyform, Parent, temp_br,branch2, branch_dir2, temp_poly2,...
            i) ;
     
            R2 = [R2; v_length+1]; %takes the index of starting point of
            %new branch
            Radius_Array = [Radius_Array, R2];
            Radius_Array =sortrows(Radius_Array',2)'; %sorts radius array 
            %according to the indeces where the corresponding branches start
            
            v_length = size(VesselSkeleton,2); %update v_length to include 
            %branching in branches
     
        end
    
    
     end
     
     
     i = i+1;   
    
end


temp_Rank = [];
 
for j= 1:size(Radius_Array,2)
    temp_Rank = [temp_Rank, VesselSkeleton(4,Radius_Array(2,j))];
end
 %Radius Array contains the radius and its correspoding rank
 %this is designed so that the radius lookup is based on rank.
 Radius_Array = [Radius_Array; temp_Rank];
 VesselSkeleton = [VesselSkeleton; Parent];
  
 

end