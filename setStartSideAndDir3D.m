function [xp,yp, zp, v] =  setStartSideAndDir3D(xlocs,ylocs,zlocs)
faceindex = 5; %randi(6,1);
switch(faceindex)
    case 1 
        yp = ylocs(1);
        xp = xlocs(1)+(xlocs(end)-xlocs(1))*rand(1);
        zp = zlocs(1)+(zlocs(end)-zlocs(1))*rand(1);
        v = [0 1 0];
    case 2
        xp = xlocs(1);
        yp = ylocs(1)+(ylocs(end)-ylocs(1))*rand(1);
        zp = zlocs(1)+(zlocs(end)-zlocs(1))*rand(1);
        v = [1 0 0];
    case 3
        yp = ylocs(end);
        xp = xlocs(1)+(xlocs(end)-xlocs(1))*rand(1);
        zp = zlocs(1)+(zlocs(end)-zlocs(1))*rand(1);
        v = [0 -1 0];
    case 4
        xp = xlocs(end);
        yp = ylocs(1)+(ylocs(end)-ylocs(1))*rand(1);
        zp = zlocs(1)+(zlocs(end)-zlocs(1))*rand(1);
        v = [-1 0 0];
    case 5
        zp = zlocs(1);
        xp = xlocs(1)+(xlocs(end)-xlocs(1))*rand(1);
        yp = ylocs(1)+(ylocs(end)-ylocs(1))*rand(1);
        v = [0 0 1];
    case 6
        zp = zlocs(end);
        xp = xlocs(1)+(xlocs(end)-xlocs(1))*rand(1);
        yp = ylocs(1)+(ylocs(end)-ylocs(1))*rand(1);
        v = [0 0 -1];
end
end