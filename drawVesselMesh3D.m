function [R, d_ratio] = drawVesselMesh3D(Vessel, Radius_Array,butterOrder, Noise, nx, ny,nz, dx, dy,dz )
%Vessel is the described centreline in the xy space as is.
%This script is supposed to give us different the drawing of the vessel
%when accounting for anisotropy
%nx smapling points in x; dx resolution in x
%Similary for ny and dy

xlocs = dx*(-nx/2:nx/2);
ylocs = dy*(-ny/2:ny/2);
zlocs = dz*(-ny/2:ny/2);
%note that the space we're drawing our vessel in slightly bigger to try to
%contain the whole vessel

R = zeros(nx+1,ny+1); % zeros(size(xlocs1,2),size(ylocs,2));

[X,Y,Z] = meshgrid(xlocs,ylocs,zlocs);


[d_ratio, loc_dr] = distanceFromVessel3DMesh(Vessel, xlocs, ylocs,zlocs, Radius_Array);

Radius = assignRadiusToVoxel(loc_dr,d_ratio);

distance = d_ratio.*Radius;

sigma1 = 2;
sigma2 = 1;
% d_ratio = real(d_ratio);
%intensity = 1./(1+(d_ratio).^butterOrder); %fluorescence, butterworth
intensity = exp(-(d_ratio).^2); %Gaussian, can be improved by dicarding values  2 sigmas away
 

R = intensity + Noise*randn(size(intensity));
end 