NBigSteps = 8;
npoints = 20;
butterOrder = 6;
Radius = 1;
Noise = 0;
nx = 100; %nx and ny are the sampling
ny = 100;
nz = 100;
dx = 1; %micron
dy = 1;
dz = 1;
segment_size = 50;
BigAng = 0;
SmallAng = 60; 
conformity = 0.95;

xlocs = dx*(-nx/2:nx/2);
ylocs = dy*(-ny/2:ny/2);
zlocs = dz*(-ny/2:ny/2);

[X,Y,Z] = meshgrid(xlocs,ylocs,zlocs);


[Vessel, Radius_Array, polyform] = create_3D_vessel...
    (npoints, NBigSteps,...
    butterOrder, segment_size, Radius, BigAng, SmallAng, conformity, ...
    dx , dy ,dz,Noise );

hr=plot3(Vessel(1,:),Vessel(2,:),Vessel(3,:),'r.');
set(hr,'LineWidth',3);
hold on;

%[R, d_ratio] = drawVesselMesh3D(Vessel, Radius_Array,butterOrder, Noise, nx, ny,nz, dx, dy,dz );

